<?php

use App\Http\Controllers\ArticleController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [App\Http\Controllers\HomeController::class, 'index']);

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::group(['middleware' => ['auth']], function () {
    Route::post('/article', [ArticleController::class, 'store'])->name('article.store');
    Route::get('/article/create', [ArticleController::class, 'create'])->name('article.create');
    Route::patch('/article/{article}', [ArticleController::class, 'update'])->name('article.update');
    Route::delete('/article/{article}', [ArticleController::class, 'destroy'])->name('article.destroy');
    Route::get('/article/{article}/edit', [ArticleController::class, 'edit'])->name('article.edit');
});

Route::prefix('article')->group(function () {
    Route::get('/', [ArticleController::class, 'index'])->name('article.index');
    Route::get('/{article}', [ArticleController::class, 'show'])->name('article.show');
});

Route::resource('user', \App\Http\Controllers\UserController::class, ['except' => ['show']])->middleware('can:isAdmin');

Route::get('/login/google', [\App\Http\Controllers\GoogleLoginController::class, 'login'])->name('google.login');
Route::get('/login/google/callback', [\App\Http\Controllers\GoogleLoginController::class, 'callback']);

