<?php

namespace App\Http\Controllers;

use App\Http\Requests\ArticleUpdateRequest;
use App\Models\Article;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Str;
use Illuminate\View\View;

class ArticleController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(): View
    {
        if (Gate::allows('isModerator')) {
            $articles = Article::where('status', 'under_moderation')->get();
        } elseif (Gate::allows('isCreator')) {
            $articles = Article::all();
        } elseif (Gate::allows('isAdmin')) {
            $articles = Article::all();
        } else {
            $articles = Article::where('status', 'published')->get();
        }

        return view('article.list', compact('articles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create(): View
    {
        return view('article/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        // Validate posted form data
        $validated = $request->validate([
            'title' => 'required|string|unique:articles|min:5|max:100',
            'body' => 'required|string|min:5|max:2000',
        ]);

        // Create slug from title
        $validated['slug'] = Str::slug($validated['title'], '-');

        // User Id
        $validated['user_id'] = Auth::user()->getAuthIdentifier();

        //Add article status
        $validated['status'] = $request->input('status');

        // Create and save post with validated data
        $article = Article::create($validated);

        // Redirect the user to the created post with a success notification
        return redirect()->route('article.show', $article->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  Article  $article
     * @return Response
     */
    public function show(int $id): View
    {
        $article = Article::findOrFail($id);
        // Pass current post to view
        return view('article.show', compact('article'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit(Article $article): View
    {
        return view('article.create', compact('article'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Article  $article
     * @return Response
     */
    public function update(ArticleUpdateRequest $request, Article $article)
    {
        $article->update($request->all());

        return redirect(route('article.index', [$article->slug]))->with('notification', 'Article updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy(Article $article)
    {
        // Delete the specified Post
        $article->delete();

        // Redirect user with a deleted notification
        return redirect(route('article.index'))->with('notification', '"' . $article->title .  '" deleted!');
    }
}
