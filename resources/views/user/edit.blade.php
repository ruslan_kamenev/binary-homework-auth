@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <h1>{{ $user->name }}</h1>

            <div>
                <p>
                    {{ $user->role }}
                </p>
            </div>
        </div>
        <form method="POST" action="{{ route('user.update', $user->id) }}">
            @method('PATCH')
            @csrf
            <div class="my-3">
                <div class="form-check">
                    <input id="draft" name="role" type="radio" checked value="creator">
                    <label class="form-check-label" for="draft">Creator role</label>
                </div>
                <div class="form-check">
                    <input id="draft" name="role" type="radio" value="moderator">
                    <label class="form-check-label" for="draft">Moderator role</label>
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>

    </div>
@endsection
