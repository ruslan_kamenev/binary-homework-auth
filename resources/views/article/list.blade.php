@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Title</th>
                    <th scope="col">Body</th>
                    <th scope="col">Status</th>
                    @canany(['isAdmin', 'isModerator', 'isCreator'])
                        <th scope="col">Actions</th>
                    @endcanany
                </tr>
                </thead>
                <tbody>
                @foreach($articles as $article)
                    <tr>
                        <td scope="row">
                            {{ $article->id }}
                        </td>
                        <td>
                            <a href="{{ route('article.show', $article->id) }}">
                                {{ $article->title }}
                            </a>
                        </td>
                        <td>
                            {{ strlen($article->body) > 50 ? substr($in,0,50)."..." : $article->body }}
                        </td>
                        <td>
                            {{ $article->status }}
                        </td>
                        @canany(['isAdmin', 'isModerator', 'isCreator'])
                            <td>
                            @switch(\Illuminate\Support\Facades\Auth::user()->role)
                                @case('admin')
                                    <a href="{{ route('article.edit', $article) }}">Edit</a>
                                @break
                                @case('moderator')
                                    <a href="{{ route('article.edit', $article) }}">Edit</a>
                                @break
                                @case('creator')
                                    @if(\Illuminate\Support\Facades\Auth::id() === $article->user_id and $article->status !== 'published')
                                        <a href="{{ route('article.edit', $article) }}">Edit</a>
                                    @endif
                                @break
                                @default
                            @endswitch
                            </td>
                        @endcanany
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

@endsection
