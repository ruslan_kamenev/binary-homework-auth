@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            @if(isset($article))
                <form method="POST" action="{{ route('article.update', $article) }}">
                @method('PATCH')
            @else
                <form method="POST" action="{{ route('article.store') }}">
            @endif
                @csrf
                <div class="mb-3">
                    <label for="title">Title</label>
                    <input type="text" name="title" id="title" class="form-control" value="{{ isset($article) ? $article->title : '' }}" />
                </div>

                <div class="mb-3">
                    <label for="body">Body</label>
                    <textarea name="body" id="body" class="form-control">{{ isset($article) ? $article->body : '' }}</textarea>
                </div>

                <div class="mb-3">
                    Select an action
                </div>
                <div class="my-3">
                    <div class="form-check">
                        <input id="draft" name="status" type="radio" checked value="draft">
                        <label class="form-check-label" for="draft">Send to draft</label>
                    </div>
                    @canany(['isCreator', 'isAdmin'])
                            <div class="form-check">
                                <input id="draft" name="status" type="radio" checked value="unpublished">
                                <label class="form-check-label" for="draft">Send to unpublished</label>
                            </div>
                    @endcanany
                    @can('isCreator')
                    <div class="form-check">
                            <input id="moderate" name="status" type="radio" checked value="under_moderation">
                            <label class="form-check-label" for="moderate">Send to moderation</label>
                    </div>
                    @endcan
                    @canany(['isModerator', 'isAdmin'])
                        <div class="form-check">
                            <input id="draft" name="status" type="radio" checked value="published">
                            <label class="form-check-label" for="draft">Publish</label>
                        </div>
                    @endcanany
                </div>

                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>

@endsection
